const request = require('supertest')(require('@app'));
const { expect } = require('chai');

describe('POST /creators/sign-up', () => {
  const payload = {
    firstName: 'Chad',
    lastName: 'Smith',
    password: 'password',
    isVerified: true
  };
  it('creates a creator account and returns the token', async () => {
    return request
      .post('/creators')
      .send({
        ...payload,
        email: 'demo@email.com',
        username: 'demo'
      })
      .then(res => {
        expect(res.body.token).to.be.a('string');
      });
  });

  it('Fails when creating account with dup email', () => {
    return request
      .post('/creators')
      .send({
        ...payload,
        email: 'chadsmith146@gmail.com',
        username: 'chad'
      })
      .expect(409);
  });

  it('Fails when creating account with dup username', () => {
    return request
      .post('/creators')
      .send({
        ...payload,
        email: 'demo1@email.com',
        username: 'chadsmith'
      })
      .expect(409);
  });
});
