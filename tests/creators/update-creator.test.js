const request = require('supertest')(require('@app'));

describe('UPDATE /creators/:id', () => {
  const CREATOR_ID = 'f0ae3cff-a173-4ed9-b99e-2f0c29ca1891';
  it('updates a creator', () => {
    return request
      .put('/creators/' + CREATOR_ID)
      .send({
        username: 'buckstoncheevers',
        firstName: 'Buckston',
        lastName: 'Cheevers',
        email: 'buckstoncheevers@gmail.com'
      })
      .expect(200);
  });
});
