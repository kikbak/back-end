const request = require('supertest')(require('@app'));

describe('GET /creators', () => {
  it('gets all of the creators', () => {
    return request.get('/creators').expect(200);
  });
});
