const request = require('supertest')(require('@app'));
const { expect } = require('chai');

describe('POST /creators/login', () => {
  it('login for a creator account', () => {
    return request
      .post('/creators/login')
      .send({
        email: 'chadsmith146@gmail.com',
        password: 'password'
      })
      .expect(200);
  });

  it('fails with an incorrect password', () => {
    return request
      .post('/creators/login')
      .send({
        email: 'chadsmith146@gmail.com',
        password: 'WRONG'
      })
      .expect(401);
  });

  it('returns creator with token and without password', async () => {
    return request
      .post('/creators/login')
      .send({
        email: 'chadsmith146@gmail.com',
        password: 'password'
      })
      .then(res => {
        expect(res.body.token).to.be.a('string');
        expect(res.body.password).to.equal(undefined);
      });
  });
});
