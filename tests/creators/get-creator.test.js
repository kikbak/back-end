const request = require('supertest')(require('@app'));
const tokens = require('../token.helper');
const { expect } = require('chai');

describe('GET /creators/:id', () => {
  const CREATOR_ID = 'f6433dec-5b4a-4aa8-b504-bb1399d20488';

  it('gets current creator with auth token', () => {
    return request
      .get('/creators/me')
      .set('Authorization', tokens.creator)
      .expect(200)
      .then(res => {
        expect(res.body.token).to.be.a('string');
      });
  });

  it('gets creator by id', () => {
    return request
      .get('/creators/' + CREATOR_ID)
      .set('Authorization', tokens.creator)
      .expect(200);
  });
});
