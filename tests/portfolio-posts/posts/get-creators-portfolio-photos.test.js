const request = require('supertest')(require('@app'));
const tokens = require('../../token.helper');

describe('GET /protfolio-posts?userId ', () => {
  const CREATOR_ID = 'f0ae3cff-a173-4ed9-b99e-2f0c29ca1891';

  it('Gets the creators portfolio posts', () => {
    const response = request
      .get(`/portfolio-posts?createdBy=${CREATOR_ID}`)
      .set('Authorization', tokens.creator)
      .expect(200);

    return response;
  });
});
