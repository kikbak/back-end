const request = require('supertest')(require('@app'));
const tokens = require('../../token.helper');

describe('DELETE /portfolio-posts/:id', () => {
  const PORTFOLIO_POST_ID = 'd6dfda6c-5585-11e9-8647-d663bd873d93';

  it('Removes a portfolio post', () => {
    const response = request
      .delete(`/portfolio-posts/${PORTFOLIO_POST_ID}`)
      .set('Authorization', tokens.creator)
      .expect(200);

    return response;
  });
});
