const request = require('supertest')(require('@app'));
const tokens = require('../../token.helper');

describe('PUT /portfolio-posts/:id', () => {
  const PORTFOLIO_POST_ID = 'd6dfda6c-5585-11e9-8647-d663bd873d93';

  it('Updates a portfolio post', () => {
    const response = request
      .put(`/portfolio-posts/${PORTFOLIO_POST_ID}`)
      .send({
        caption: 'updated caption'
      })
      .set('Authorization', tokens.creator)
      .expect(200);

    return response;
  });
});
