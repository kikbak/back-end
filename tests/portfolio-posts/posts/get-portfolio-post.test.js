const request = require('supertest')(require('@app'));
const tokens = require('../../token.helper');

describe('GET /portfolio-posts/:id', () => {
  const PORTFOLIO_POST_ID = 'd6dfda6c-5585-11e9-8647-d663bd873d93';

  it('Gets a portfolio post', () => {
    const response = request
      .get(`/portfolio-posts/${PORTFOLIO_POST_ID}`)
      .set('Authorization', tokens.creator)
      .expect(200);

    return response;
  });
});
