const request = require('supertest')(require('@app'));
const tokens = require('../../token.helper');

describe('POST /portfolio-posts', () => {
  it('Creates a portfolio post', () => {
    const response = request
      .post(`/portfolio-posts`)
      .send({
        imgUrl: 'https://s3-us-west-2.amazonaws.com/name-tba/avi.png',
        caption: 'Art from sumer of 2019'
      })
      .set('Authorization', tokens.creator)
      .expect(200);

    return response;
  });
});
