const request = require('supertest')(require('@app'));
const tokens = require('../../token.helper');

const LIKED_POST_ID = 'd6dfda6c-5585-11e9-8647-d663bd873d93';
describe('GET /portfolio-posts/:likedPost/favorites', () => {
  it('creates post favorite', () => {
    const response = request
      .get(`/portfolio-posts/${LIKED_POST_ID}/favorites`)
      .set('Authorization', tokens.creator)
      .expect(200);

    return response;
  });
});
