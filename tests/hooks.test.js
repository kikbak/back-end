const knex = require('@helpers/knex.helper');

before('sets the  testing environment', () => {
  process.env.TESTING = true;
});

beforeEach('places test objects in the database', () => {
  return knex.seed.run({
    directory: './database/seeds'
  });
});

after('kills database connection', () => {
  return knex.destroy();
});
