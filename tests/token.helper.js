const jwt = require('jsonwebtoken');

module.exports = {
  creator: jwt.sign(
    { id: 'f0ae3cff-a173-4ed9-b99e-2f0c29ca1891', type: 'login' },
    process.env.SECRET_KEY
  )
};
