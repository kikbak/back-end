exports.up = async knex => {
  const UUID = knex.raw('uuid_generate_v1()');
  const NOW = knex.raw('now()');
  await knex.schema.createTable('portfolio_posts', table => {
    table
      .uuid('id')
      .primary()
      .defaultsTo(UUID);
    table.string('imgUrl').notNullable();
    table
      .uuid('createdBy')
      .references('creators.id')
      .onDelete('CASCADE');
    table
      .boolean('archived')
      .notNullable()
      .defaultsTo(false);
    table
      .dateTime('created')
      .notNullable()
      .defaultsTo(NOW);
    table.text('caption');
    table.integer('favorites').defaultsTo(0);
  });
};

exports.down = async knex => {
  await knex.schema.dropTableIfExists('portfolio_posts');
};
