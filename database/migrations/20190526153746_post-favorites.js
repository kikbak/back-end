exports.up = async knex => {
  const UUID = knex.raw('uuid_generate_v1()');
  const NOW = knex.raw('now()');

  await knex.schema.createTable('portfolio_post_favorites', table => {
    table
      .uuid('id')
      .primary()
      .defaultsTo(UUID);
    table
      .uuid('createdBy')
      .references('creators.id')
      .onDelete('CASCADE');
    table
      .boolean('archived')
      .notNullable()
      .defaultsTo(false);
    table.dateTime('created').defaultsTo(NOW);
    table
      .uuid('likedPost')
      .references('portfolio_posts.id')
      .notNullable()
      .onDelete('CASCADE');
  });

  //Drop the favorites count from the portfolio post table
  await knex.schema.table('portfolio_posts', table => {
    table.dropColumn('favorites');
  });
};

exports.down = async knex => {
  await knex.schema.dropTableIfExists('portfolio_post_favorites');

  await knex.schema.table('portfolio_posts', table => {
    table.integer('favorites').defaultsTo(0);
  });
};
