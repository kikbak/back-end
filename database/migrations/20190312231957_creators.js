exports.up = async knex => {
  await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');

  const UUID = knex.raw('uuid_generate_v1()');
  const NOW = knex.raw('now()');
  const NO_IMAGE = 'https://s3-us-west-2.amazonaws.com/name-tba/logo.png';

  await knex.schema.createTable('creators', table => {
    table
      .uuid('id')
      .primary()
      .defaultsTo(UUID);
    table
      .text('email')
      .unique()
      .notNullable();
    table
      .text('username')
      .unique()
      .notNullable();
    table
      .boolean('archived')
      .defaultsTo(false)
      .notNullable();
    table
      .boolean('isVerified')
      .notNullable()
      .defaultsTo(false);
    table
      .dateTime('created')
      .notNullable()
      .defaultsTo(NOW);
    table.text('firstName').notNullable();
    table.text('lastName').notNullable();
    table.text('aviUrl').defaultsTo(NO_IMAGE);
    table.text('password');
  });
};

exports.down = async knex => {
  await knex.schema.dropTableIfExists('creators');
};
