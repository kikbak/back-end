exports.seed = async knex => {
  // ADD NEW TABLES AT THE TOP
  await knex('portfolio_post_favorites').del();
  await knex('portfolio_posts').del();
  await knex('creators').del();
};
