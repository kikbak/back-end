const bcrypt = require('bcryptjs');

exports.seed = async knex => {
  const password = await bcrypt.hash('password', 10);
  await knex('creators').insert([
    {
      id: 'f0ae3cff-a173-4ed9-b99e-2f0c29ca1891',
      firstName: 'Chad',
      lastName: 'Smith',
      username: 'chadsmith',
      email: 'chadsmith146@gmail.com',
      created: new Date(),
      isVerified: true,
      password: password,
      aviUrl: 'https://s3-us-west-2.amazonaws.com/name-tba/jane-photo.jpg'
    },
    {
      id: '2077c86b-c65e-4093-a602-f1a731366f25',
      firstName: 'Duplicate',
      lastName: 'User',
      username: 'duplicateuser',
      email: 'test@email.com',
      created: new Date(),
      isVerified: false,
      password: password,
      aviUrl: 'https://s3-us-west-2.amazonaws.com/name-tba/logo.png'
    },
    {
      id: 'f6433dec-5b4a-4aa8-b504-bb1399d20488',
      firstName: 'Jae',
      lastName: 'Yu',
      email: 'jaezo.yu@gmail.com',
      username: 'jzo',
      created: new Date(),
      isVerified: true,
      password: password,
      aviUrl: 'https://s3-us-west-2.amazonaws.com/name-tba/logo.png'
    }
  ]);
};
