exports.seed = async knex => {
  await knex('portfolio_posts').insert([
    {
      id: 'd6dfda6c-5585-11e9-8647-d663bd873d93',
      imgUrl: 'https://s3-us-west-2.amazonaws.com/name-tba/jane-photo.jpg',
      createdBy: 'f0ae3cff-a173-4ed9-b99e-2f0c29ca1891',
      caption: 'This is a logo'
    },
    {
      id: 'beba72b8-55ca-11e9-8647-d663bd873d93',
      imgUrl: 'https://s3-us-west-2.amazonaws.com/name-tba/jane-photo.jpg',
      createdBy: 'f0ae3cff-a173-4ed9-b99e-2f0c29ca1891',
      caption: 'this is another logo'
    }
  ]);
};
