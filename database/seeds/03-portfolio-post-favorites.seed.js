exports.seed = async knex => {
  await knex('portfolio_post_favorites').insert([
    {
      id: 'b0c3e710-8000-11e9-bc42-526af7764f64',
      createdBy: 'f6433dec-5b4a-4aa8-b504-bb1399d20488',
      likedPost: 'beba72b8-55ca-11e9-8647-d663bd873d93'
    },
    {
      id: '4dbc2cb4-96f2-11e9-bc42-526af7764f64',
      createdBy: 'f6433dec-5b4a-4aa8-b504-bb1399d20488',
      likedPost: 'd6dfda6c-5585-11e9-8647-d663bd873d93'
    },
    {
      id: '8ec5684c-96f2-11e9-bc42-526af7764f64',
      createdBy: 'f0ae3cff-a173-4ed9-b99e-2f0c29ca1891',
      likedPost: 'd6dfda6c-5585-11e9-8647-d663bd873d93'
    }
  ]);
};
