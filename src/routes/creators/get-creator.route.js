const router = require('express').Router();
const authorize = require('@middlewares/auth.middleware');
const Creators = require('@models/creators.model');
const { validate, joi } = require('@middlewares/validate.middleware');

/**
 * @api {get} /creators/:id Get creator
 * @apiGroup creators
 *
 * @apiParam (Params) {String} id creator id (use "me" to get current creator)
 */
router.get(
  '/:id',
  validate({
    params: {
      id: joi.string().required()
    }
  }),
  authorize(),
  async (req, res, next) => {
    try {
      const creatorId = req.params.id === 'me' ? req.creator.id : req.params.id;

      res.status(200).json(
        await Creators.findOne(creatorId, {
          includeToken: req.params.id === 'me'
        })
      );
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
