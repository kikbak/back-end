const router = require('express').Router();
const { validate, joi } = require('@middlewares/validate.middleware');
const Creators = require('@models/creators.model');
const bcrypt = require('bcryptjs');

/**
 * @api {post} /creator/login Login
 * @apiGroup Creator
 *
 * @apiParam (Body) {String} [email] user email
 * @apiParam (Body) {String} [password] password
 */
router.post(
  '/login',
  validate({
    body: {
      email: joi
        .string()
        .lowercase()
        .email()
        .required(),
      password: joi.string()
    }
  }),
  async (req, res, next) => {
    try {
      if (!req.body.password) {
        return res.status(400).json({ message: 'Password is required.' });
      }

      const CREATOR = await Creators.findOne(
        { email: req.body.email },
        { ENCRYPT: true }
      );

      if (!CREATOR) {
        return res.status(404).json({ message: 'Email not found.' });
      }

      if (req.body.password) {
        if (
          !CREATOR.password ||
          !(await bcrypt.compare(req.body.password, CREATOR.password))
        ) {
          return res.status(401).json({ message: 'Incorrect password.' });
        }
      }

      const creator = await Creators.findOne(CREATOR.id, {
        includeToken: true
      });

      return res.status(200).json(creator);
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
