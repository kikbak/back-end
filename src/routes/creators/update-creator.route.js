const router = require('express').Router();
const app = require('@app');
const Creators = require('@models/creators.model');
const { validate, joi } = require('@middlewares/validate.middleware');

/**
 * @api {put} /creators/:id Updates a Creator's information
 * @apiGroup Creators
 *
 * @apiParam (Params) {String} id Creator's ID
 * @apiParam (Body) {String} username Creator's username
 * @apiParam (Body) {String} firstName Creator's first name
 * @apiParam (Body) {String} lastName Creator's last name
 * @apiParam (Body) {String} email Creator's email
 * @apiParam (Body) {String} password Creator's password
 */
router.put(
  '/:id',
  validate({
    params: {
      id: joi
        .string()
        .uuid()
        .required()
    },
    body: {
      aviUrl: joi.string().trim(),
      username: joi
        .string()
        .lowercase()
        .trim(),
      firstName: joi.string().trim(),
      lastName: joi.string().trim(),
      email: joi
        .string()
        .lowercase()
        .email(),
      password: joi.string().min(8)
    }
  }),
  async (req, res, next) => {
    try {
      const updatedCreator = await Creators.update(req.params.id, {
        aviUrl: req.body.aviUrl,
        username: req.body.username,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password
      });

      if (req.body.aviUrl) app.io.emit('event:creator-profile-changed');

      res.status(200).json(updatedCreator);
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
