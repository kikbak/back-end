const router = require('express').Router();
const Creators = require('@models/creators.model');

/**
 * @api {get} /creators Get Creators
 * @apiGroup Creators
 */
router.get('/', async (req, res, next) => {
  try {
    res.status(200).json(await Creators.find());
  } catch (err) {
    next(err);
  }
});

module.exports = router;
