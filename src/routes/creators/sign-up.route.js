const router = require('express').Router();
const Creators = require('@models/creators.model');
const { validate, joi } = require('@middlewares/validate.middleware');

/**
 * @api {post} /sign-up Creates a new account for a creator
 * @apiGroup Creators
 *
 * @apiParam (Body) {String} username creator's username
 * @apiParam (Body) {String} firstName first name
 * @apiParam (Body) {String} lastName last name
 * @apiParam (Body) {String} email email
 * @apiParam (Body) {String} phoneNumber phone number
 * @apiParam (Body) {Boolean} isVerified is the creator verified
 * @apiParam (Body) {String} password password
 *
 */
router.post(
  '/',
  validate({
    body: {
      username: joi
        .string()
        .lowercase()
        .trim()
        .required(),
      firstName: joi
        .string()
        .trim()
        .required(),
      lastName: joi
        .string()
        .trim()
        .required(),
      email: joi
        .string()
        .lowercase()
        .email()
        .required(),
      password: joi
        .string()
        .min(8)
        .required(),
      isVerified: joi.boolean()
    }
  }),
  async (req, res, next) => {
    try {
      if (await Creators.findOne({ email: req.body.email })) {
        return res.status(409).json({
          message: 'Someone is already using that email.'
        });
      }

      if (await Creators.findOne({ username: req.body.username })) {
        return res.status(409).json({
          message: 'Someone is already using that username.'
        });
      }

      const UNSAFE_CREATOR = await Creators.create({
        username: req.body.username,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        isVerified: req.body.isVerified
      });

      const creator = await Creators.findOne(UNSAFE_CREATOR.id, {
        includeToken: true
      });

      return res.status(200).json(creator);
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
