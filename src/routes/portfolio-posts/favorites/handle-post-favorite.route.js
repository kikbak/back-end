const router = require('express').Router({ mergeParams: true });
const authorize = require('@middlewares/auth.middleware');
const { validate, joi } = require('@middlewares/validate.middleware');
const PostFavorites = require('@models/portfolio-post-favorites.model');

/**
 * @api {post} /posts/:likedPost/favorites Creates a post favorite
 * @apiGroup Post Favorites
 *
 * @apiParams (params) {String} likedPost ID of the liked post
 */
router.post(
  '/',
  authorize(),
  validate({
    params: {
      likedPost: joi
        .string()
        .uuid()
        .required()
    }
  }),
  async (req, res, next) => {
    try {
      const isLiked = await PostFavorites.findOne({
        'portfolio_post_favorites.likedPost': req.params.likedPost,
        'portfolio_post_favorites.createdBy': req.creator.id
      });

      if (!isLiked) {
        const createdPostFavorite = await PostFavorites.create({
          createdBy: req.creator.id,
          likedPost: req.params.likedPost
        });

        res.status(200).json(createdPostFavorite);
      } else if (isLiked.archived === true) {
        const updatedPostFavorite = await PostFavorites.update(isLiked.id, {
          archived: false
        });

        res.status(200).json(updatedPostFavorite);
      } else {
        const updatedPostFavorite = await PostFavorites.update(isLiked.id, {
          archived: true
        });

        res.status(200).json(updatedPostFavorite);
      }
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
