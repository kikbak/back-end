const router = require('express').Router({ mergeParams: true });
const authorize = require('@middlewares/auth.middleware');
const { validate, joi } = require('@middlewares/validate.middleware');
const PostFavorites = require('@models/portfolio-post-favorites.model');

/**
 * @api {get} /portolio-posts/:likedPost/favorites gets a post's favorites
 * @apiGroup Post Favorites
 *
 * @apiParams (params) {String} likedPost ID of the liked post
 */
router.get(
  '/',
  authorize(),
  validate({
    params: {
      likedPost: joi
        .string()
        .uuid()
        .required()
    }
  }),
  async (req, res, next) => {
    try {
      const postFavorites = await PostFavorites.find({
        likedPost: req.params.likedPost
      });

      res.status(200).json(postFavorites);
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
