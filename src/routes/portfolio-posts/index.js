const router = require('express').Router();

router.use('/', [
  require('./posts/create-portfolio-post.route'),
  require('./posts/get-creators-portfolio-posts.route'),
  require('./posts/get-portfolio-post.route'),
  require('./posts/update-portfolio-post.route'),
  require('./posts/remove-portfolio-post.route')
]);

router.use('/:likedPost/favorites', [
  require('./favorites/handle-post-favorite.route'),
  require('./favorites/get-post-favorites')
]);

module.exports = router;
