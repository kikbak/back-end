const router = require('express').Router();
const authorize = require('@middlewares/auth.middleware');
const { joi, validate } = require('@middlewares/validate.middleware');
const PortfolioPosts = require('@models/portfolio-posts.model');

/**
 * @api (get) /portfolio-posts creates a portfolio post.
 * @apiGroup Portfolio Posts
 *
 * @apiParams (body) {String} imgUrl Images URL for in the bucket
 * @apiParams (body) {String} caption Images Caption
 */
router.post(
  '/',
  authorize(),
  validate({
    body: {
      imgUrl: joi
        .string()
        .trim()
        .required(),
      caption: joi.string().trim()
    }
  }),
  async (req, res, next) => {
    try {
      const newPost = await PortfolioPosts.create({
        createdBy: req.creator.id,
        imgUrl: req.body.imgUrl,
        caption: req.body.caption
      });

      res.status(200).json(newPost);
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
