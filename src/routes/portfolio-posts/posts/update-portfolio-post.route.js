const router = require('express').Router();
const authorize = require('@middlewares/auth.middleware');
const { joi, validate } = require('@middlewares/validate.middleware');
const PortfolioPosts = require('@models/portfolio-posts.model');

/**
 * @api (put) /portfolio-posts/:id creates a portfolio post.
 * @apiGroup Portfolio Posts
 *
 * @apiParams (Params) {String} id Post ID
 * @apiParams (body) {String} caption Images Caption
 * @apiParams (body) {Integer} favorites Image's favorites
 */
router.put(
  '/:id',
  authorize(),
  validate({
    params: {
      id: joi
        .string()
        .uuid()
        .required()
    },
    body: {
      caption: joi.string().trim(),
      favorites: joi.number().integer()
    }
  }),
  async (req, res, next) => {
    try {
      const updatedPost = await PortfolioPosts.update(req.params.id, {
        favorites: req.body.favorites,
        caption: req.body.caption
      });

      res.status(200).json(updatedPost);
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
