const router = require('express').Router();
const authorize = require('@middlewares/auth.middleware');
const { joi, validate } = require('@middlewares/validate.middleware');
const PortfolioPosts = require('@models/portfolio-posts.model');

/**
 * @api (put) /portfolio-posts/:id deletes a portfolio post.
 * @apiGroup Portfolio Posts
 *
 * @apiParams (Params) {String} id Post ID
 */
router.delete(
  '/:id',
  authorize(),
  validate({
    params: {
      id: joi
        .string()
        .uuid()
        .required()
    }
  }),
  async (req, res, next) => {
    try {
      const removedPost = await PortfolioPosts.update(req.params.id, {
        archived: true
      });

      res.status(200).json(removedPost);
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
