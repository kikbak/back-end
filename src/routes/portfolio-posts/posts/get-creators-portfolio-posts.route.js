const router = require('express').Router();
const authorize = require('@middlewares/auth.middleware');
const PortfolioPosts = require('@models/portfolio-posts.model');

/**
 * @api (get) /portfolio-posts?creatorId= gets the creators portfolio posts.
 * @apiGroup Portfolio Posts
 *
 */
router.get('/', authorize(), async (req, res, next) => {
  try {
    const creatorsPosts = await PortfolioPosts.find(req.query);
    res.status(200).json(creatorsPosts);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
