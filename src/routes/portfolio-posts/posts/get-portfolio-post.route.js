const router = require('express').Router();
const authorize = require('@middlewares/auth.middleware');
const { joi, validate } = require('@middlewares/validate.middleware');
const PortfolioPosts = require('@models/portfolio-posts.model');

/**
 * @api (get) /portfolio-posts/:id gets a portfolio post.
 * @apiGroup Portfolio Posts
 *
 * @apiParams (Params) {String} id Portfolio Post ID
 */
router.get(
  '/:id',
  authorize(),
  validate({
    params: {
      id: joi
        .string()
        .uuid()
        .required()
    }
  }),
  async (req, res, next) => {
    try {
      const portfolioPost = await PortfolioPosts.findOne(req.params.id);
      res.status(200).json(portfolioPost);
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
