const router = require('express').Router();

router.get('/', (req, res) => {
  res.send('Welcome to The Cave.');
});

router.use('/creators', [
  require('./creators/login.route'),
  require('./creators/sign-up.route'),
  require('./creators/get-creators.route'),
  require('./creators/get-creator.route'),
  require('./creators/update-creator.route')
]);

router.use('/img-uploads', [require('./img-uploads/img-upload.route')]);

router.use('/portfolio-posts', require('./portfolio-posts'));

module.exports = router;
