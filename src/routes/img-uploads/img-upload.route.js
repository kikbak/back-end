const router = require('express').Router();
const upload = require('@helpers/multer.helper');
const aws = require('@middlewares/file-upload.middleware');

router.post('/', upload.single('file'), aws.doUpload);

module.exports = router;
