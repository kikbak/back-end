const knex = require('@helpers/knex.helper');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const TABLE_NAME = 'creators';

const FIELDS = [
  'id',
  'username',
  'email',
  'archived',
  'created',
  'firstName',
  'lastName',
  'isVerified',
  'aviUrl',
  knex.raw('CONCAT("firstName", \' \', "lastName") AS name')
];

const ENCRYPT_FIELDS = FIELDS.concat('password');

const findOne = (module.exports.findOne = (query, options = {}) => {
  return knex(TABLE_NAME)
    .where(typeof query === 'string' ? { id: query } : query)
    .andWhere({ archived: false })
    .first(options.ENCRYPT ? ENCRYPT_FIELDS : FIELDS)
    .then(object => {
      if (object) {
        return {
          ...object,
          token: options.includeToken
            ? jwt.sign({ id: object.id, type: 'login' }, process.env.SECRET_KEY)
            : undefined
        };
      }
    });
});

module.exports.update = (id, fields, options = {}) => {
  fields.password = fields.password
    ? bcrypt.hashSync(fields.password, 10)
    : undefined;

  return knex(TABLE_NAME)
    .where({ id: id })
    .update(fields, 'id')
    .then(res => findOne(res[0], options));
};

module.exports.create = (object, options = {}) => {
  object.password = object.password
    ? bcrypt.hashSync(object.password, 10)
    : undefined;

  return knex(TABLE_NAME)
    .insert(object, 'id')
    .then(res => findOne(res[0], options));
};

module.exports.find = (query, options = {}) => {
  return knex(TABLE_NAME)
    .where(query || {})
    .andWhere({ archived: false })
    .orderBy('created')
    .select(options.ENCRYPT ? ENCRYPT_FIELDS : FIELDS);
};
