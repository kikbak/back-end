const knex = require('@helpers/knex.helper');

const TABLE_NAME = 'portfolio_post_favorites';

const FIELDS = [
  //Favorites Fields
  'portfolio_post_favorites.id',
  'portfolio_post_favorites.createdBy',
  'portfolio_post_favorites.archived',
  'portfolio_post_favorites.created',
  'portfolio_post_favorites.likedPost',

  // Creators Fields
  'creators.username AS username'
];

const findOne = (module.exports.findOne = query => {
  const postFavorite = knex(TABLE_NAME)
    .where(
      typeof query === 'string'
        ? { 'portfolio_post_favorites.id': query }
        : query
    )
    .join('creators', 'creators.id', 'portfolio_post_favorites.createdBy')
    .first(FIELDS);

  return postFavorite;
});

module.exports.find = query => {
  const postFavorites = knex(TABLE_NAME)
    .where(query || {})
    .join('creators', 'creators.id', 'portfolio_post_favorites.createdBy')
    .andWhere({ 'portfolio_post_favorites.archived': false })
    .orderBy('created')
    .select(FIELDS);

  return postFavorites;
};

module.exports.update = (id, fields) => {
  const updatedFavorite = knex(TABLE_NAME)
    .where({ 'portfolio_post_favorites.id': id })
    .update(fields, 'id')
    .then(res => findOne(res[0]));

  return updatedFavorite;
};

module.exports.create = object => {
  const newFavorite = knex(TABLE_NAME)
    .insert(object, 'id')
    .then(res => findOne(res[0]));

  return newFavorite;
};
