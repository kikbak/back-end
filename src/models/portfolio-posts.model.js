const knex = require('@helpers/knex.helper');

const TABLE_NAME = 'portfolio_posts';

const FIELDS = [
  // Portfolio Posts Fields
  'portfolio_posts.id',
  'portfolio_posts.created',
  'portfolio_posts.createdBy',
  'portfolio_posts.archived',
  'portfolio_posts.caption',
  'portfolio_posts.imgUrl',

  // Creators Fields
  'creators.username AS username'
];

const findOne = (module.exports.findOne = query => {
  const portfolioPost = knex(TABLE_NAME)
    .where(typeof query === 'string' ? { 'portfolio_posts.id': query } : query)
    .join('creators', 'creators.id', 'portfolio_posts.createdBy')
    .andWhere({ 'portfolio_posts.archived': false })
    .first(FIELDS);

  return portfolioPost;
});

module.exports.find = query => {
  // the subquery get the favorite count, so we don't have as many api calls on the front-end
  const portfolioPosts = knex(TABLE_NAME)
    .where(query || {})
    .andWhere({ 'portfolio_posts.archived': false })
    .join('creators', 'creators.id', 'portfolio_posts.createdBy')
    .leftJoin(
      knex('portfolio_post_favorites')
        .select(knex.raw('count(*) as ??', ['favoriteCount']), 'likedPost')
        .from('portfolio_post_favorites')
        .as('favorites')
        .where({ 'portfolio_post_favorites.archived': false })
        .groupBy('likedPost'),
      'favorites.likedPost',
      'portfolio_posts.id'
    )
    .orderBy('portfolio_posts.created')
    .select('portfolio_posts.*', 'favorites.favoriteCount');

  return portfolioPosts;
};

module.exports.update = (id, fields) => {
  return knex(TABLE_NAME)
    .where({ 'portfolio_posts.id': id })
    .update(fields, 'id')
    .then(res => findOne(res[0]));
};

module.exports.create = object => {
  const newPortfolioPost = knex(TABLE_NAME)
    .insert(object, 'id')
    .then(res => findOne(res[0]));

  return newPortfolioPost;
};
