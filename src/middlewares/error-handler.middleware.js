module.exports = (err, req, res, next) => {
  if (err.name === 'JsonWebTokenError') {
    return res.status(400).json({ message: 'Invalid token.' });
  } else if (err.name === 'TokenExpiredError') {
    return res.status(400).json({ message: 'Token expired.' });
  } else if (err.type === 'entity.too.large') {
    return res.status(413).json({ message: 'Too large - must be under 5MB.' });
  }

  next(err);
};
