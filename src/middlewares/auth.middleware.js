const jwt = require('jsonwebtoken');
const Creators = require('@models/creators.model');

module.exports = permission => {
  return async (req, res, next) => {
    try {
      if (!req.header('Authorization')) {
        return res
          .status(400)
          .json({ message: 'Missing authorization header.' });
      }

      const { id, type } = jwt.verify(
        req.header('Authorization'),
        process.env.SECRET_KEY
      );

      if (type !== 'login') {
        return res.status(401).json({ message: 'Invalid token.' });
      }

      const creator = await Creators.findOne(id);

      if (permission) {
        throw new Error('Invalid permission specified on route.');
      }

      req.creator = creator;

      next();
    } catch (err) {
      next(err);
    }
  };
};
