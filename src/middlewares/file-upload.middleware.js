const s3 = require('@helpers/s3.helper');
const uuidv1 = require('uuid/v1');

exports.doUpload = (req, res) => {
  const s3Client = s3.s3Client;
  const params = s3.uploadParams;
  const photoUuid = uuidv1();
  const fileType = req.file.originalname.match(/\.[a-z]+$/);

  params.Key = photoUuid + fileType;
  params.Body = req.file.buffer;

  s3Client.upload(params, err => {
    if (err) {
      res.status(500).json({ error: 'Error -> ' + err });
    }
    res.json({
      fileName: photoUuid + fileType
    });
  });
};
