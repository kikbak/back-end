require('module-alias/register');
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const helmet = require('helmet');
const compression = require('compression');
const cors = require('cors');
const { json } = require('body-parser');
const routes = require('./routes');
const httpsRedirect = require('./middlewares/https-redirect.middleware');
const errorHandler = require('./middlewares/error-handler.middleware');

// Lets the server trust the headers Heroku gives
app.enable('trust proxy');

app.use(httpsRedirect);
app.use(compression());
app.use(helmet());
app.use(cors());
app.use(json({ limit: '5mb' }));

app.use('/', routes);

app.use(errorHandler);

module.exports.io = io;
module.exports = app;

if (!module.parent) {
  app.listen(process.env.PORT, () => {
    console.log(`Up on port ${process.env.PORT}.`);
  });
}
